import logo from './logo.svg';
import React from 'react'
import Cookies from 'js-cookie';

// Pretty table styling adapted from:
//    https://colorlib.com/wp/template/fixed-header-table/

import './css/select2.min.css'
import './css/animate.css'
import './css/perfect-scrollbar.css'
import './css/main.css'

import './App.css';

// Render a single cloud item row
class CloudItem extends React.Component {
    render() {

        let dist = Math.round(this.props.item.distance);
        dist = isNaN(dist)? '' : dist;

      return (
        <tr className="row100 body">
	  <td className="cell100 column1">{this.props.item.cloud_name}</td>
	  <td className="cell100 column2">{this.props.item.cloud_description}</td>
	  <td className="cell100 column3">{this.props.item.geo_region}</td>
	  <td className="cell100 column5">{this.props.item.geo_latitude} / {this.props.item.geo_longitude}</td>
	  <td className="cell100 column4">{dist}</td>
        </tr>
      );
   }
}

class CloudsList extends React.Component {
    render() {
      return (
        <div className="limiter">
        <div className="container-table100">
          <div className="wrap-table100">
          <div className="table100 ver2 m-b-110">
          <div className="table100-head">
            <table>
              <thead>
                <tr className="row100 head">
                  <th className="cell100 column1">Cloud name</th>
                  <th className="cell100 column2">Description</th>
                  <th className="cell100 column3">Region</th>
                  <th className="cell100 column4">Lat/Lon</th>
                  <th className="cell100 column5">Distance(Km)</th>
                </tr>
              </thead>
            </table>
          </div>
          <div className="table100-body js-pscroll">
            <table>
              <tbody>
	        {this.props.list.map(elem => (<CloudItem key={elem.cloud_name} item={elem}/>))}
              </tbody>
            </table>
          </div>
          </div>
          </div>
        </div>
        </div>
      );
    }
}

class Filter extends React.Component {

    constructor(props) {
        super(props);
	this.onToggle = this.props.onToggle;
    }

    render() {
        let filters = this.props.filters;
        return (
          <div className="filter-block">
	  <b className="filter-title">Filter by providers:</b> 
	      {Object.keys(filters).map(provider => (
		 <span key={provider} className="filter-item">
		   <input type="checkbox" id={provider} name={provider} checked={filters[provider]}
		      onChange={() => {this.onToggle(provider)}}/>
                   <label htmlFor={provider}>{provider}</label>
                 </span>))}
          </div>
       );
    }
}

// GeoLocation distance calculation, taken and reformated from:
//     https://www.geodatasource.com/developers/javascript

function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 === lat2) && (lon1 === lon2)) {
	return 0;
    }
    else {
	var radlat1 = Math.PI * lat1/180;
	var radlat2 = Math.PI * lat2/180;
	var theta = lon1-lon2;
	var radtheta = Math.PI * theta/180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	if (dist > 1) {
	    dist = 1;
	}
	dist = Math.acos(dist);
	dist = dist * 180/Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit==="K") { dist = dist * 1.609344 }
	if (unit==="N") { dist = dist * 0.8684 }
	return dist;
    }
}

// Geolocation class adapted from:
//     https://dev.to/codebucks/how-to-get-user-s-location-in-react-js-1691

class GeoLocation extends React.Component {

    constructor(props) {
        super(props);
        console.log(props);
        this.onLocation = this.props.onLocation;
        this.state = {message: ""};
    }

    componentDidMount() {
        console.log("GeoLocation component mounted");
	if (navigator.geolocation) {

	  console.log("Geolocation supported by browser");
          navigator.permissions
	  .query({name: "geolocation"})
	  .then((result) => {

              if (result.state === "granted") {
		  console.log(result.state);

                  // Grab the user's location and propagate to parent.
		  navigator.geolocation.getCurrentPosition((position) => {
                          let lat = position.coords.latitude;
                          let lon = position.coords.longitude;
                          console.log("Latitude is :", lat);
                          console.log("Longitude is :", lon);
                          this.onLocation(lat, lon);
                  });
	      } else if (result.state === "prompt") {
	          console.log(result.state);
	      } else if (result.state === "denied") {
                  console.log("GeoLocation denied by the user");
	          this.setState({message: "Sorry, without the GeoLocation, there is no sorting! Enable it in browser's Settings -> Privacy and security -> Site settings -> Permissions -> Location"});
	      }
	      result.onchange = function () {
	          console.log(result.state);
	      };
          });
	} else {
            this.setState({message: "Sorry, GeoLocation is not supported, there is no sorting!"});
	}
    }

    render() {
	if (this.state.message.length > 0) {
            return(<div className="geolocation-error">{this.state.message}</div>);
	} else {
	    return '';
	}
    }
}

// Fetches the list of clouds list from backend API and renders it.
class Clouds extends React.Component {

    constructor(props) {
        super(props);
	this.state = {error: "", clouds: [], filters: {}, locationResolved: false, lat: 0, lon: 0};
    }

    componentDidMount() {

        // Fetch the current list of clouds from backend API
	fetch(this.props.url, {
	      method: "get",
	      credentials: "same-origin",
	      headers: {
		  "X-CSRFToken": Cookies.get("csrftoken"),
		  "Accept": "application/json",
		  "Content-Type": "application/json"
	       }
             })
	    .then(response => {console.log(response); return response.json();})
	    .then(data => {
		let providers = {};
                data.forEach(elem => providers[elem.provider] = true);
                this.setState({clouds: data, filters: providers});
	    })
            .catch(error => {
                console.log(error);
                this.setState({error: error});
	     });
    }

    onFilterToggle(provider) {

        let providers = this.state.filters;
        providers[provider] = !providers[provider];
	this.setState({filters: providers});
    }

    onLocation(lat, lon) {

        // With the location available, sort the clouds list in distance.
        let clouds = this.state.clouds;
        clouds.forEach(elem => elem.distance = distance(parseFloat(elem.geo_latitude),
			                                parseFloat(elem.geo_longitude),
                                                        parseFloat(lat), parseFloat(lon), 'K'));
	let sorted = this.state.clouds.sort((a, b) => a.distance - b.distance);

        // Update states with the updated clouds list
        this.setState({locationResolved: true, lat: lat, lon: lon, clouds: sorted});
    }

    render() {

        // Render error message if API fetch fails.
        if (this.state.error) {
	    return (
		    <div className="Error-message">
                        Backend API failed: {this.state.error.toString()}
                    </div>
	    );
        }

        // Otherwise, filter the list according to current selection.
        let providers = this.state.filters;
        let clouds = this.state.clouds;
	let filtered = clouds.filter(elem => providers[elem.provider]);

        // Render the list of clouds.
        return (
          <div>
            <GeoLocation onLocation={(lat, lon) => {this.onLocation(lat, lon)}}/>
	    <Filter filters={providers} onToggle={provider => {this.onFilterToggle(provider)}}/>
            <CloudsList list={filtered}/>
          </div>
      );
   }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3 className="title">Clouds selection home work</h3>
      </header>
      <Clouds url="http://localhost:8000/clouds/"/>
    </div>
  );
}

export default App;
