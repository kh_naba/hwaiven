from django.db import models

class Clouds(models.Model):
    """Model that caches clouds list"""

    cloud_name = models.CharField(max_length=512, unique=True)
    cloud_description = models.CharField(max_length=1024)
    geo_region = models.CharField(max_length=128)
    geo_longitude = models.FloatField()
    geo_latitude = models.FloatField()
    provider = models.CharField(max_length=512)

    def __str__(self):
        return self.cloud_name
