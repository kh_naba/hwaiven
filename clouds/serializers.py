# serializers.py

from rest_framework import serializers
from .models import Clouds

class CloudsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Clouds
        fields = ('cloud_name', 'cloud_description', 'geo_region', 'geo_longitude', 'geo_latitude', 'provider')
