# views.py

from rest_framework import viewsets
from .serializers import CloudsSerializer
from .models import Clouds
from django.shortcuts import render
import requests

class CloudsViewSet(viewsets.ModelViewSet):
    """Lists the currently stored clouds in the database"""

    # Retrieve all existing clouds from database
    queryset = Clouds.objects.all()
    serializer_class = CloudsSerializer

    def get_queryset(self):

        # If database is empty, fill the cache from original source
        if len(self.queryset) <= 0:
            response = requests.get("https://api.aiven.io/v1/clouds")
            print("Aiven API query response: ", response)

            if response.status_code == 200:
                data = response.json()

                # Store the response in our cache database
                for cloud_attribs in data['clouds']:

                    # Augment the record with Provider found in Description
                    desc = cloud_attribs['cloud_description']
                    begin = desc.find('-')
                    end = desc.find(':')
                    if (end > (begin + 2)):
                        cloud_attribs['provider'] = desc[begin + 2 : end]

                    # Create and store the augmented cloud record
                    cloud = Clouds(**cloud_attribs)
                    cloud.save()

                # Update the queryset with the updated cache
                self.queryset = Clouds.objects.all()

        return self.queryset
