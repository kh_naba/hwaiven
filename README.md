# hwaiven

## Installation

1. Make sure python3 and pip are installed in the system. If not, follow the instructions here: https://realpython.com/installing-python/
3. Make sure Node.js is installed. If not, see https://nodejs.org/en/download/package-manager/
2. Install required modules for python and node:
>>>
    pip install django
    pip install djangorestframework
    pip install django-cors-headers
    npm install create-react-app
    npm install js-cookie --save
>>>

3. Clone and checkout this git repository
>>>
    git clone https://gitlab.com/kh_naba/hwaiven.git
>>>

## Launch backend and frontend

1. Launch Django backend, which will be available at http://localhost:8000/
>>>
    cd hwaiven
    ./manage.py migrate (only first time)
    ./manage.py runserver
>>>

2. Launch React.js frontend, which will be available at http://localhost:3000/
>>>
    cd frontend
    npm start
>>>

## Screenshot: backend REST API, augmented Aiven cloud API

![alt text](screenshots/backend.png "Backend screenshot")

## Screenshot: frontend, ordered by distance computed using Geolocation API

![alt text](screenshots/frontend.png "Frontend screenshot")

## Screenshot, frontend, filter support

![alt text](screenshots/frontend-filtered.png "Frontend screenshot")

## Screenshot, frontend, error if browser geolocation is not enabled.

![alt text](screenshots/frontend-error.png "Frontend screenshot")
